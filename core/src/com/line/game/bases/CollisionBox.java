package com.line.game.bases;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

public class CollisionBox {

    public static final int TOP = 1;
    public static final int BOTTOM = 2;
    public static final int LEFT  = 4;
    public static final int RIGHT = 8;
    public static final int ALL = 15;

    public final CollisionLine[]collisionLines;

    public CollisionBox(Vector2 origin, float width, float height, int flags) {
        final int size = (flags & 1) + (flags >> 1 & 1) + (flags >> 2 & 1) + (flags >> 3 & 1);
        collisionLines = new CollisionLine[size];

        int index = 0;
        if ((flags & 1) == 1) {
            collisionLines[index++] = new CollisionLine(new Vector2(origin.x, origin.y), width, 0);
        }

        if ((flags & 2) == 2) {
            collisionLines[index++] = new CollisionLine(new Vector2(origin.x + width, origin.y - height), width, 180);
        }

        if ((flags & 4) == 4) {
            collisionLines[index++] = new CollisionLine(new Vector2(origin.x, origin.y - height), height, 90);
        }

        if ((flags & 8) == 8) {
            collisionLines[index++] = new CollisionLine(new Vector2(origin.x + width, origin.y), height, 270);
        }
    }

    public void render(Renderer renderer) {
        final ShapeRenderer shapeRenderer = renderer.shapeRenderer;
        Color[] colors = { Color.RED, Color.BLUE, Color.GREEN, Color.ORANGE };
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        for (int i = 0; i < collisionLines.length; i++) {
            shapeRenderer.setColor(colors[i]);
            shapeRenderer.line(collisionLines[i].start, collisionLines[i].end);
        }
        shapeRenderer.end();
    }

}
