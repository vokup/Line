package com.line.game.maps;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.line.game.bases.CollisionLine;
import com.line.game.bases.Renderer;
import com.line.game.controllers.*;
import com.line.game.models.*;
import com.line.game.screens.PlayScreen;
import com.line.game.utils.GameUtils;
import com.line.game.views.*;

import java.util.Random;

public class MapMission extends Map {

    private final PlayScreen screen;

    private final Random random;
    private final FontView fontTimerView;
    private final FontView fontScoreView;
    private final FontController fontScoreController;
    private final FontController fontTimeController;

    private final int START_TIME = 60;
    private int mScore = 0;
    private long mStartTime = 0;
    private int mCurTime = 0;
    private float mBallSpeed = 4.0f;

    public MapMission(PlayScreen screen) {
        this.screen = screen;

        random = new Random(System.currentTimeMillis());

        fontTimerView = new FontView(100);
        fontScoreView = new FontView(48);
        fontTimeController = new FontController(new FontModel());
        fontTimeController.setPosition(PlayScreen.SCREEN_WIDTH / 2 - 50, PlayScreen.SCREEN_HEIGHT / 2 + 30);
        fontTimeController.setText("00");

        fontScoreController = new FontController(new FontModel());
        fontScoreController.setPosition(PlayScreen.SCREEN_WIDTH / 2 - 24, 115);
        fontTimeController.setText("00");

        create();
    }

    private void create() {
        // screen bound
        addCollisionLine(new Vector2(0, PlayScreen.SCREEN_HEIGHT),
                new Vector2(PlayScreen.SCREEN_WIDTH, PlayScreen.SCREEN_HEIGHT));
        addCollisionLine(new Vector2(0, 0),
                new Vector2(PlayScreen.SCREEN_WIDTH, 0));
        addCollisionLine(new Vector2(0, 0),
                new Vector2(0, PlayScreen.SCREEN_HEIGHT));
        addCollisionLine(new Vector2(PlayScreen.SCREEN_WIDTH, 0),
                new Vector2(PlayScreen.SCREEN_WIDTH , PlayScreen.SCREEN_HEIGHT));

        BlockModel blockModel = new BlockModel(PlayScreen.SCREEN_WIDTH / 2 - 100, PlayScreen.SCREEN_HEIGHT / 2 - 100, 200, 200, Color.valueOf("#28b463"));
        addBlock(blockModel);

        addCollisionLine(new Vector2(PlayScreen.SCREEN_WIDTH / 2 - 100, PlayScreen.SCREEN_HEIGHT / 2 + 100),
                         new Vector2(PlayScreen.SCREEN_WIDTH / 2 + 100, PlayScreen.SCREEN_HEIGHT / 2 + 100));
        addCollisionLine(new Vector2(PlayScreen.SCREEN_WIDTH / 2 - 100, PlayScreen.SCREEN_HEIGHT / 2 - 100),
                         new Vector2(PlayScreen.SCREEN_WIDTH / 2 + 100, PlayScreen.SCREEN_HEIGHT / 2 - 100));
        addCollisionLine(new Vector2(PlayScreen.SCREEN_WIDTH / 2 - 100, PlayScreen.SCREEN_HEIGHT / 2 - 100),
                         new Vector2(PlayScreen.SCREEN_WIDTH / 2 - 100, PlayScreen.SCREEN_HEIGHT / 2 + 100));
        addCollisionLine(new Vector2(PlayScreen.SCREEN_WIDTH / 2 + 100, PlayScreen.SCREEN_HEIGHT / 2 - 100),
                         new Vector2(PlayScreen.SCREEN_WIDTH / 2 + 100, PlayScreen.SCREEN_HEIGHT / 2 + 100));

        addSpawn(new SpawnModel(PlayScreen.SCREEN_WIDTH / 2, 100, 40, Color.ORANGE));

        addTarget(new TargetModel((PlayScreen.SCREEN_WIDTH / 2) - (PlayScreen.SCREEN_WIDTH / 4), PlayScreen.SCREEN_HEIGHT - 100, 30, Color.RED));
        addTarget(new TargetModel((PlayScreen.SCREEN_WIDTH / 2) + (PlayScreen.SCREEN_WIDTH / 4), PlayScreen.SCREEN_HEIGHT - 100, 30, Color.BLUE));

        generateBall();

        mStartTime = System.currentTimeMillis();
    }

    private void generateBall() {
        Color randColor = targetList.get(random.nextInt(targetList.size())).getColor();
        BallModel ballModel = new BallModel(PlayScreen.SCREEN_WIDTH / 2, 40, 20, randColor);
        BallController ballController = addBall(ballModel);
        int angle = random.nextInt(360);
        ballController.setSpeed((float)(Math.cos(angle * Math.PI / 180.0) * mBallSpeed), (float)(Math.sin(angle * Math.PI / 180.0) * mBallSpeed));
    }

    @Override
    public void update(float delta) {
        if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
            if (!isLeftButtonPressed) {
                isLeftButtonPressed = true;
                isPlayerCollisionLineSet = false;
                setPlayerCollisionLineStartPosition(Gdx.input.getX(), PlayScreen.SCREEN_HEIGHT - Gdx.input.getY());
            }
        } else {
            if (isLeftButtonPressed) {
                isLeftButtonPressed = false;
                setPlayerCollisionLineEndPosition(Gdx.input.getX(), PlayScreen.SCREEN_HEIGHT - Gdx.input.getY());
                isPlayerCollisionLineSet = true;

                updatePlayerCollisionLine();
            }
        }

        for (BallController ball : this.ballList) {
            ball.update(delta);

            for (TargetController target : this.targetList) {
                if (GameUtils.isCircleAndCircleIntersect(ball.getPosition(), ball.getRadius(), target.getPosition(), target.getRadius())) {
                    if (target.getColor() == ball.getColor()) {
                        mScore++;
                        mBallSpeed = 4.0f + (mScore / 2) * 0.5f;
                    } else {
                        this.screen.requestToEndScreen(mScore);
                    }
                    this.ballList.remove(ball);
                    generateBall();
                }
            }

            if (isPlayerCollisionLineSet) {
                if (GameUtils.isLineAndCircleIntersect(null, playerCollisionLine.start, playerCollisionLine.end, ball.getPosition(), ball.getRadius())) {
                    Vector2 reflectionVector = GameUtils.getReflectionVector(ball.getSpeed(), playerCollisionLine.norm);
                    ball.setSpeed(reflectionVector);
                }
            }

            for (CollisionLine line : this.collisionLineList) {
                if (GameUtils.isLineAndCircleIntersect(null, line.start, line.end, ball.getPosition(), ball.getRadius())) {
                    Vector2 reflectionVector = GameUtils.getReflectionVector(ball.getSpeed(), line.norm);
                    ball.setSpeed(reflectionVector);
                }
            }
        }

        mCurTime = START_TIME - (int)(System.currentTimeMillis() - mStartTime) / 1000;
        if (mCurTime <= 0) {
            this.screen.requestToEndScreen(mScore);
        }
    }

    @Override
    public void render(float delta, Renderer renderer) {
        super.render(delta, renderer);

        fontTimeController.setText((mCurTime < 10 ? "0" : "") + Integer.toString(mCurTime));
        fontTimeController.render(renderer, fontTimerView);

        fontScoreController.setText((mScore < 10 ? "0" : "") + Integer.toString(mScore));
        fontScoreController.render(renderer, fontScoreView);

    }

    @Override
    public void dispose() {

    }
}
