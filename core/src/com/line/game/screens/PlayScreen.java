package com.line.game.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.line.game.LineGame;
import com.line.game.bases.BaseScreen;
import com.line.game.bases.Renderer;
import com.line.game.maps.Map;
import com.line.game.maps.MapMission;

public class PlayScreen extends BaseScreen {

    public static final int REQUEST_END_SCREEN = 1;
    public static final int SCREEN_WIDTH = 480;
    public static final int SCREEN_HEIGHT = 720;

    private final ShapeRenderer mShapeRenderer;
    private final Map mCurMap;
    private final Renderer mRenderer;

    public PlayScreen(LineGame game) {
        super(game);

        mShapeRenderer = new ShapeRenderer();
        mShapeRenderer.setAutoShapeType(true);
        mShapeRenderer.setColor(Color.BLACK);

        mRenderer = new Renderer(getSpriteBatch(), mShapeRenderer);

        mCurMap = new MapMission(this);
    }

    public void requestToEndScreen(int score) {
        game.setScreen(new EndScreen(game, score));
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void render(float delta) {
        super.render(delta);

        mCurMap.render(delta, mRenderer);
    }

    @Override
    public void dispose() {
        super.dispose();

        mShapeRenderer.dispose();
    }

    @Override
    public void resize(int width, int height) {

    }
}
