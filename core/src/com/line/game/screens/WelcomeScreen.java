package com.line.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.line.game.LineGame;
import com.line.game.bases.BaseScreen;
import com.line.game.bases.Renderer;
import com.line.game.controllers.FontController;
import com.line.game.maps.Map;
import com.line.game.maps.MapWelcome;
import com.line.game.models.FontModel;
import com.line.game.views.FontView;

public class WelcomeScreen extends BaseScreen {

    public static final int SCREEN_WIDTH = 480;
    public static final int SCREEN_HEIGHT = 720;

    private final ShapeRenderer mShapeRenderer;
    private final Map map;
    private final Renderer renderer;

    public WelcomeScreen(LineGame game) {
        super(game);

        mShapeRenderer = new ShapeRenderer();
        mShapeRenderer.setAutoShapeType(true);
        mShapeRenderer.setColor(Color.BLACK);

        renderer = new Renderer(getSpriteBatch(), mShapeRenderer);

        map = new MapWelcome();
    }

    @Override
    public void update(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            game.setScreen(new PlayScreen(game));
        }
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        map.render(delta, renderer);
    }

    @Override
    public void dispose() {
        super.dispose();

        map.dispose();
    }

    @Override
    public void resize(int width, int height) {

    }
}