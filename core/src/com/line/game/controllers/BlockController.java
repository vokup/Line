package com.line.game.controllers;

import com.line.game.bases.Renderer;
import com.line.game.models.BlockModel;
import com.line.game.views.BlockView;

public class BlockController extends Controller<BlockModel, BlockView> {

    public BlockController(BlockModel model) {
        super(model);
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void render(Renderer renderer, BlockView view) {
        view.render(renderer, model);
    }
}
