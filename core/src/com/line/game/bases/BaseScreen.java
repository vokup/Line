package com.line.game.bases;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.line.game.LineGame;

public abstract class BaseScreen implements Screen {

    protected final LineGame game;
    private final SpriteBatch mSpriteBatch;

    public SpriteBatch getSpriteBatch() { return mSpriteBatch; }

    public BaseScreen(LineGame game) {
        this.game = game;
        mSpriteBatch = new SpriteBatch();
    }

    @Override
    public void show() { }

    public abstract void update(float delta);

    @Override
    public void render(float delta) {
        update(delta);
    }

    @Override
    public abstract void resize(int width, int height);

    @Override
    public void pause() {  }

    @Override
    public void resume() { }

    @Override
    public void hide() { }

    @Override
    public void dispose() {
        mSpriteBatch.dispose();
    }
}