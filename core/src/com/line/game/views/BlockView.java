package com.line.game.views;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.line.game.bases.Renderer;
import com.line.game.models.BlockModel;

public class BlockView extends View<BlockModel> {
    @Override
    public void render(Renderer renderer, BlockModel model) {
        final ShapeRenderer shapeRenderer = renderer.shapeRenderer;
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(model.color);
        shapeRenderer.rect(model.position.x, model.position.y,  model.width, model.height);
        shapeRenderer.end();
    }
}
