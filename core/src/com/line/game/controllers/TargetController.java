package com.line.game.controllers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.line.game.bases.Renderer;
import com.line.game.models.TargetModel;
import com.line.game.views.TargetView;

public class TargetController extends Controller<TargetModel, TargetView> {

    public TargetController(TargetModel model) {
        super(model);
    }

    public Color getColor() {
        return model.color;
    }

    public Vector2 getPosition() {
        return model.position;
    }

    public float getRadius() {
        return model.radius;
    }

    @Override
    public void update(float delta) {

    }

    public void render(Renderer renderer, TargetView view) {
        view.render(renderer, model);
    }
}
