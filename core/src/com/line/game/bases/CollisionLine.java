package com.line.game.bases;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

public class CollisionLine {
    public final Vector2 norm;
    public final Vector2 start;
    public final Vector2 end;

    public CollisionLine(Vector2 origin, float length, float rotateInDegree) {
        final double degreeInRadian = rotateInDegree * Math.PI / 180.0;
        final double degreeInRadianPlus90  = (rotateInDegree + 90) * Math.PI / 180.0;
        start = new Vector2(origin);
        end = new Vector2(origin.x + (float)(length * Math.cos(degreeInRadian)), origin.y + (float)(length * Math.sin(degreeInRadian)));
        norm = new Vector2((float)Math.cos(degreeInRadianPlus90), (float)Math.sin(degreeInRadianPlus90));
            /*System.out.println();
            System.out.println("(" + norm.x + "," + norm.y + ")");
            System.out.println("(" + p1.x + "," + p1.y + ")");
            System.out.println("(" + p2.x + "," + p2.y + ")");*/
    }

    public void render(Renderer renderer) {
        final ShapeRenderer shapeRenderer = renderer.shapeRenderer;
        Color[] colors = { Color.RED, Color.BLUE, Color.GREEN, Color.ORANGE };
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.line(start, end);
        shapeRenderer.end();
    }
}