package com.line.game.controllers;

import com.line.game.bases.Renderer;
import com.line.game.models.FontModel;
import com.line.game.views.FontView;

public class FontController extends Controller<FontModel, FontView> {
    public FontController(FontModel model) {
        super(model);
    }

    public void setPosition(float x, float y) {
        model.position.set(x, y);
    }

    public void setText(String text) {
        model.text = text;
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void render(Renderer renderer, FontView view) {
        view.render(renderer, model);
    }
}
