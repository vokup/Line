package com.line.game.maps;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.line.game.bases.BaseScreen;
import com.line.game.bases.CollisionLine;
import com.line.game.bases.Renderer;
import com.line.game.controllers.BallController;
import com.line.game.controllers.FontController;
import com.line.game.models.BallModel;
import com.line.game.models.FontModel;
import com.line.game.screens.PlayScreen;
import com.line.game.screens.WelcomeScreen;
import com.line.game.utils.GameUtils;
import com.line.game.views.FontView;

import java.util.Random;

public class MapWelcome extends Map {

    private final FontView fontView;
    private final FontController fontController;

    public MapWelcome() {

        // screen bound
        addCollisionLine(new Vector2(0, PlayScreen.SCREEN_HEIGHT),
                         new Vector2(PlayScreen.SCREEN_WIDTH, PlayScreen.SCREEN_HEIGHT));
        addCollisionLine(new Vector2(0, 0),
                         new Vector2(PlayScreen.SCREEN_WIDTH, 0));
        addCollisionLine(new Vector2(0, 0),
                         new Vector2(0, PlayScreen.SCREEN_HEIGHT));
        addCollisionLine(new Vector2(PlayScreen.SCREEN_WIDTH, 0),
                         new Vector2(PlayScreen.SCREEN_WIDTH , PlayScreen.SCREEN_HEIGHT));

        fontView = new FontView(42);
        fontController = new FontController(new FontModel());
        fontController.setText("Press Enter to Start");
        fontController.setPosition(30, WelcomeScreen.SCREEN_HEIGHT / 2);

        Random random = new Random(System.currentTimeMillis());
        BallModel ballModel = new BallModel(PlayScreen.SCREEN_WIDTH / 2, 40, 20, new Color(random.nextFloat(), random.nextFloat(), random.nextFloat(), 0));
        BallController ballController = addBall(ballModel);
        int angle = random.nextInt(360);
        ballController.setSpeed((float)(Math.cos(angle * Math.PI / 180.0) * 8.0f), (float)(Math.sin(angle * Math.PI / 180.0) * 8.0f));
        addBall(ballModel);
    }

    @Override
    public void update(float delta) {
        for (BallController ball : this.ballList) {
            ball.update(delta);

            for (CollisionLine line : collisionLineList) {
                if (GameUtils.isLineAndCircleIntersect(null, line.start, line.end, ball.getPosition(), ball.getRadius())) {
                    Vector2 reflectionVector = GameUtils.getReflectionVector(ball.getSpeed(), line.norm);
                    ball.setSpeed(reflectionVector);
                }
            }
        }
    }

    @Override
    public void render(float delta, Renderer renderer) {
        super.render(delta, renderer);

        fontController.render(renderer, fontView);
    }

    @Override
    public void dispose() {

    }
}
