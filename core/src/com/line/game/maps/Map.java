package com.line.game.maps;

import com.badlogic.gdx.math.Vector2;
import com.line.game.bases.BaseScreen;
import com.line.game.bases.CollisionLine;
import com.line.game.bases.Renderer;
import com.line.game.controllers.BallController;
import com.line.game.controllers.BlockController;
import com.line.game.controllers.SpawnController;
import com.line.game.controllers.TargetController;
import com.line.game.models.BallModel;
import com.line.game.models.BlockModel;
import com.line.game.models.SpawnModel;
import com.line.game.models.TargetModel;
import com.line.game.views.BallView;
import com.line.game.views.BlockView;
import com.line.game.views.SpawnView;
import com.line.game.views.TargetView;

import java.util.LinkedList;
import java.util.List;

public abstract class Map {

    protected final List<BallController> ballList;
    protected final List<BlockController> blockList;
    protected final List<CollisionLine> collisionLineList;
    protected final List<TargetController> targetList;
    protected final List<SpawnController> spawnList;

    protected CollisionLine playerCollisionLine;
    protected final Vector2 playerPressingPoint;
    protected final Vector2 playerReleasingPoint;
    protected boolean isLeftButtonPressed = false;
    protected boolean isPlayerCollisionLineSet = false;

    private final BallView ballView;
    private final BlockView blockView;
    private final SpawnView spawnView;
    private final TargetView targetView;

    public Map() {
        ballView = new BallView();
        blockView = new BlockView();
        spawnView = new SpawnView();
        targetView = new TargetView();

        this.ballList = new LinkedList<BallController>();
        this.blockList = new LinkedList<BlockController>();
        this.collisionLineList = new LinkedList<CollisionLine>();
        this.targetList = new LinkedList<TargetController>();
        this.spawnList = new LinkedList<SpawnController>();

        this.playerCollisionLine = new CollisionLine(new Vector2(0, 0), 0, 180);
        this.playerPressingPoint = new Vector2();
        this.playerReleasingPoint = new Vector2();
    }

    public void setPlayerCollisionLineStartPosition(float x, float y) {
        playerPressingPoint.set(x, y);
    }

    public void setPlayerCollisionLineEndPosition(float x, float y) {
        playerReleasingPoint.set(x, y);

    }

    public void updatePlayerCollisionLine() {
        float dx = playerReleasingPoint.x - playerPressingPoint.x;
        float dy = playerReleasingPoint.y - playerPressingPoint.y;
        playerCollisionLine = new CollisionLine(playerPressingPoint, Vector2.len(dx, dy), (float)(Math.atan2(dy, dx) * 180.0 / Math.PI));
    }

    public BallController addBall(BallModel model) {
        BallController controller = new BallController(model);
        this.ballList.add(controller);
        return controller;
    }

    public BlockController addBlock(BlockModel model) {
        BlockController controller = new BlockController(model);
        this.blockList.add(controller);
        return controller;
    }

    public SpawnController addSpawn(SpawnModel model) {
        SpawnController controller = new SpawnController(model);
        this.spawnList.add(controller);
        return controller;
    }

    public TargetController addTarget(TargetModel model) {
        TargetController controller = new TargetController(model);
        this.targetList.add(controller);
        return controller;
    }

    public void addCollisionLine(Vector2 start, Vector2 end) {
        float dx = end.x - start.x;
        float dy = end.y - start.y;
        this.collisionLineList.add(new CollisionLine(start, Vector2.len(dx, dy), (float)(Math.atan2(dy, dx) * 180.0 / Math.PI)));
    }

    public abstract void update(float delta);

    public void render(float delta, Renderer renderer) {
        update(delta);


        for (SpawnController spawn : this.spawnList) {
            spawn.render(renderer, spawnView);
        }

        for (TargetController target : this.targetList) {
            target.render(renderer, targetView);
        }

        for (BlockController block : this.blockList) {
            block.render(renderer, blockView);
        }

        if (isPlayerCollisionLineSet) {
            playerCollisionLine.render(renderer);
        }


        for (BallController ball : this.ballList) {
            ball.render(renderer, ballView);
        }

        for (CollisionLine line : this.collisionLineList) {
            line.render(renderer);
        }
    }

    public abstract void dispose();
}
