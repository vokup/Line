package com.line.game.views;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.line.game.bases.Renderer;
import com.line.game.models.SpawnModel;
import com.line.game.models.TargetModel;

public class TargetView extends View<TargetModel> {
    @Override
    public void render(Renderer renderer, TargetModel model) {
        final ShapeRenderer shapeRenderer = renderer.shapeRenderer;
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(model.color);
        shapeRenderer.circle(model.position.x, model.position.y, model.radius);
        shapeRenderer.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.circle(model.position.x, model.position.y, model.radius);
        shapeRenderer.end();
    }
}
