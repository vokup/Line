package com.line.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.line.game.screens.EndScreen;
import com.line.game.screens.PlayScreen;
import com.line.game.screens.WelcomeScreen;

public class LineGame extends Game {

	public LineGame() {
		super();
	}

	@Override
	public void create() {
		setScreen(new WelcomeScreen(this));
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(52 / 255.0f, 152 / 255.0f, 219 / 255.0f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		super.render();
	}
}

