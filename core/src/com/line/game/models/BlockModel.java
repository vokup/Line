package com.line.game.models;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public class BlockModel {
    public final Vector2 position;
    public final float width;
    public final float height;
    public final Color color;

    public BlockModel(float x, float y, float width, float height, Color color) {
        this.position = new Vector2(x, y);
        this.width = width;
        this.height = height;
        this.color = color;
    }
}
