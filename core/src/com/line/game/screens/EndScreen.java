package com.line.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.line.game.LineGame;
import com.line.game.bases.BaseScreen;
import com.line.game.bases.Renderer;
import com.line.game.controllers.FontController;
import com.line.game.models.FontModel;
import com.line.game.views.FontView;

public class EndScreen extends BaseScreen {

    public static final int SCREEN_WIDTH = 480;
    public static final int SCREEN_HEIGHT = 720;

    private final FontView fontScoreView;
    private final FontView fontDetailView;
    private final FontController fontYourScoreIsController;
    private final FontController fontScoreController;
    private final FontController fontDetailController;
    private final Renderer renderer;

    private final int score;

    public EndScreen(LineGame game, int score) {
        super(game);
        this.score = score;
        renderer = new Renderer(getSpriteBatch(), null);
        fontScoreView = new FontView(120);
        fontDetailView = new FontView(32);

        fontYourScoreIsController = new FontController(new FontModel());
        fontYourScoreIsController.setPosition(SCREEN_WIDTH / 2 - 150, SCREEN_HEIGHT / 2 + 150);
        fontYourScoreIsController.setText("Score");

        fontScoreController = new FontController(new FontModel());
        fontScoreController.setPosition(SCREEN_WIDTH / 2 - 70, SCREEN_HEIGHT / 2 + 25);
        fontScoreController.setText((score < 10 ? "0" : "") + Integer.toString(score));

        fontDetailController = new FontController(new FontModel());
        fontDetailController.setPosition(SCREEN_WIDTH / 2 - 180, SCREEN_HEIGHT / 2 - 100);
        fontDetailController.setText("Press enter to continue");

    }

    @Override
    public void update(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            game.setScreen(new WelcomeScreen(game));
        }
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        fontYourScoreIsController.render(renderer, fontScoreView);
        fontScoreController.render(renderer, fontScoreView);
        fontDetailController.render(renderer, fontDetailView);
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    @Override
    public void resize(int width, int height) {

    }
}