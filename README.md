# LINE
LINE เป็นเกมที่ผู้เล่นจะมีลูกบอลที่ถูกระบุสีไว้ ผู้เล่นจะต้องทำการใช้การเขียนเส้นจำลองเป็นกำแพง เพื่อให้ลูกบอลเด้งไปยังเป้าหมายให้ตรงตามสีของลูกบอล

สร้างขึ้นเพื่อเป็น Project ในวิชา Object-Oriented Programming ภายในภาควิชาวิศวกรรมคอมพิวเตอร์ มหาวิทยาลัยเกษตรศาสตร์

![Screenshot of Line](https://gitlab.com/vokup/Line/-/raw/assets/preview.gif)

## Game Details
- **Creator:** [Thanapawee Phrachan](https://gitlab.com/vokup)
- **Language:** Java 8
- **Game Library:** LibGDX
- **Design Architecture:** Model-View-Controller (MVC)
- **Delevopment Time:** 1 Month
