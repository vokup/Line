package com.line.game.views;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.line.game.bases.Renderer;
import com.line.game.models.BallModel;

public class BallView extends View<BallModel> {
    @Override
    public void render(Renderer renderer, BallModel model) {
        final ShapeRenderer shapeRenderer = renderer.shapeRenderer;
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(model.color);
        shapeRenderer.circle(model.position.x, model.position.y, model.radius);
        shapeRenderer.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.circle(model.position.x, model.position.y, model.radius);
        shapeRenderer.end();
    }
}
