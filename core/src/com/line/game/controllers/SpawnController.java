package com.line.game.controllers;

import com.line.game.bases.Renderer;
import com.line.game.models.SpawnModel;
import com.line.game.views.SpawnView;

public class SpawnController extends Controller<SpawnModel, SpawnView> {

    public SpawnController(SpawnModel model) {
        super(model);
    }

    @Override
    public void update(float delta) {

    }

    public void render(Renderer renderer, SpawnView view) {
        view.render(renderer, model);
    }
}
