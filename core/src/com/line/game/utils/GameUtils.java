package com.line.game.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.math.Vector2;
import com.line.game.bases.Renderer;

public class GameUtils {
    public static Vector2 getReflectionVector(Vector2 incident, Vector2 norm) {
        Vector2 tempIncident = new Vector2(incident);
        float scalar = 2 * tempIncident.dot(norm);
        return tempIncident.sub(norm.x * scalar, norm.y * scalar);
    }

    public static boolean isCircleAndCircleIntersect(Vector2 center1, float radius1, Vector2 center2, float radius2) {
        return Vector2.len2(center2.x - center1.x, center2.y - center1.y) <= (radius1 + radius2) * (radius1 + radius2);
    }

    public static boolean isLineAndCircleIntersect(Renderer renderer, Vector2 start, Vector2 end, Vector2 center, float radius) {

        Vector2 line = new Vector2(end.x - start.x, end.y - start.y);
        Vector2 lineUnit = new Vector2(line.x / line.len(), line.y / line.len());

        /*renderer.shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        renderer.shapeRenderer.setColor(Color.PURPLE);
        renderer.shapeRenderer.line(center, start);
        renderer.shapeRenderer.end();*/

        float projLength = line.dot(center.x - start.x, center.y - start.y);

        if (projLength < 0.0f && (Vector2.len2(center.x - start.x, center.y - start.y) > (radius * radius))) {
            return false;
        }

        Vector2 projVector = new Vector2(projLength / line.len() * lineUnit.x + start.x, projLength / line.len() * lineUnit.y + start.y);
        /*renderer.shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        renderer.shapeRenderer.line(start, projVector);
        renderer.shapeRenderer.line(projVector, center);
        renderer.shapeRenderer.end();*/

        if (Vector2.len2(projLength / line.len() * lineUnit.x, projLength / line.len() * lineUnit.y) > line.len2() &&
            (Vector2.len2(end.x - center.x, end.y - center.y) > radius * radius)) {
            return false;
        }

        return Vector2.len2(projVector.x - center.x, projVector.y - center.y) <= radius * radius;
    }
}
