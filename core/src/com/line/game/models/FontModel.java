package com.line.game.models;

import com.badlogic.gdx.math.Vector2;

public class FontModel {
    public final Vector2 position;
    public String text;

    public FontModel() {
        this.position = new Vector2();
        this.text = "";
    }
}
