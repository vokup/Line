package com.line.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.line.game.bases.Renderer;
import com.line.game.models.FontModel;

public class FontView extends View<FontModel> {

    private BitmapFont bitmapFont;

    public FontView(int fontSize) {
        bitmapFont = new BitmapFont();
        final FreeTypeFontGenerator freeTypeFontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("UbuntuMono-R.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = fontSize;
        bitmapFont = freeTypeFontGenerator.generateFont(parameter);
        freeTypeFontGenerator.dispose();
    }

    @Override
    public void render(Renderer renderer, FontModel model) {
        renderer.spriteBatch.begin();
        bitmapFont.draw(renderer.spriteBatch, model.text, model.position.x, model.position.y);
        renderer.spriteBatch.end();
    }

    public void dispose() {
        bitmapFont.dispose();
    }
}
