package com.line.game.views;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.line.game.bases.Renderer;

public abstract class View<T> extends Sprite {
    public abstract void render(Renderer renderer, T model);
}
