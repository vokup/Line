package com.line.game.models;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public class TargetModel {
    public final Vector2 position;
    public final float radius;
    public final Color color;

    public TargetModel(float x, float y, float radius, Color color) {
        this.position = new Vector2(x, y);
        this.radius = radius;
        this.color = color;
    }
}
