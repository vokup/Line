package com.line.game.models;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public class BallModel {
    public final Vector2 position;
    public final Vector2 speed;
    public final float radius;
    public final Color color;

    public BallModel(float x, float y, float r, Color color) {
        this(x, y, 0, 0, r, color);
    }

    public BallModel(float x, float y, float vx, float vy, float r, Color color) {
        this.position = new Vector2(x, y);
        this.speed = new Vector2(vx, vy);
        this.radius = r;
        this.color = color;
    }
}
