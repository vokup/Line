package com.line.game.controllers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.line.game.bases.Renderer;
import com.line.game.models.BallModel;
import com.line.game.views.BallView;

public class BallController extends Controller<BallModel, BallView> {

    public BallController(BallModel model) {
        super(model);
    }

    public Vector2 getPosition() {
        return model.position;
    }

    public Vector2 getSpeed() {
        return model.speed;
    }

    public float getRadius() {
        return model.radius;
    }

    public Color getColor() {
        return model.color;
    }

    public void setSpeed(float x, float y) {
        model.speed.set(x, y);
    }

    public void setSpeed(Vector2 speed) {
        model.speed.set(speed);
    }

    @Override
    public void update(float delta) {
        model.position.add(model.speed);
    }

    public void render(Renderer renderer, BallView view) {
        view.render(renderer, model);
    }
}
