package com.line.game.controllers;

import com.line.game.bases.Renderer;

public abstract class Controller<T, V> {

    protected final T model;

    public Controller(T model) {
        this.model = model;
    }

    public abstract void update(float delta);
    public abstract void render(Renderer renderer, V view);
}
